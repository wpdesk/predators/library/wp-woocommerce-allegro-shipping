<?php

$data = array (
	'6a0f88ba-8f25-44fc-b1cf-25f45cbe8e72' =>
		array (
			'id' => '6a0f88ba-8f25-44fc-b1cf-25f45cbe8e72',
			'name' => 'Kurier wieczór',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '12.24',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'3ca09a1f-11f2-4763-974e-ae80404062c3' =>
		array (
			'id' => '3ca09a1f-11f2-4763-974e-ae80404062c3',
			'name' => 'Kurier wieczór pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '15.31',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'f041aa82-9700-4248-aed0-df298f766b25' =>
		array (
			'id' => 'f041aa82-9700-4248-aed0-df298f766b25',
			'name' => 'Odbiór w punkcie pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'd66cfec5-12f6-452b-9a0c-16a02e717a11' =>
		array (
			'id' => 'd66cfec5-12f6-452b-9a0c-16a02e717a11',
			'name' => 'Odbiór w punkcie',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'57b987de-ce2d-4e73-9b9f-a7c9ebe9611d' =>
		array (
			'id' => '57b987de-ce2d-4e73-9b9f-a7c9ebe9611d',
			'name' => 'Odbiór osobisty',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 1,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'a6e4845b-9ace-48ea-b232-918eaf11ba6c' =>
		array (
			'id' => 'a6e4845b-9ace-48ea-b232-918eaf11ba6c',
			'name' => 'Odbiór osobisty pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 1,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'7e113f1c-301a-406b-aa7a-b8654bc8c762' =>
		array (
			'id' => '7e113f1c-301a-406b-aa7a-b8654bc8c762',
			'name' => 'Allegro UPS Odbiór w Punkcie (do 06.09.2021)',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '7.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'bfe96673-283f-40b3-b6ba-70940b16f7a4' =>
		array (
			'id' => 'bfe96673-283f-40b3-b6ba-70940b16f7a4',
			'name' => 'Allegro UPS Odbiór w Punkcie (do 06.09.2021) pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '10.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'2a8cb7bf-a31b-45c3-b45e-7918ea717539' =>
		array (
			'id' => '2a8cb7bf-a31b-45c3-b45e-7918ea717539',
			'name' => 'Allegro X-press Couriers (z odbiorem zużytego akumulatora)',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '49.94',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'3d7c74e7-4d1b-4800-95a2-7918ea717539' =>
		array (
			'id' => '3d7c74e7-4d1b-4800-95a2-7918ea717539',
			'name' => 'Allegro X-press Couriers (z odbiorem zużytego akumulatora) pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '53.38',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'ffb2643b-4b90-4925-9d29-0d93ad9488a6' =>
		array (
			'id' => 'ffb2643b-4b90-4925-9d29-0d93ad9488a6',
			'name' => 'Paczka48',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'97286096-eb28-40f9-9efc-95ecbb8624ea' =>
		array (
			'id' => '97286096-eb28-40f9-9efc-95ecbb8624ea',
			'name' => 'Paczka48 pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'4dd9c904-e892-4649-bdec-5454d6b53d28' =>
		array (
			'id' => '4dd9c904-e892-4649-bdec-5454d6b53d28',
			'name' => 'Pocztex Kurier24',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'b6bf5f9c-6cc8-4a0e-ab2f-bd49e76528f5' =>
		array (
			'id' => 'b6bf5f9c-6cc8-4a0e-ab2f-bd49e76528f5',
			'name' => 'Pocztex Kurier24 pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'aa1d05e0-943b-47cb-a759-9d8c16707129' =>
		array (
			'id' => 'aa1d05e0-943b-47cb-a759-9d8c16707129',
			'name' => 'Allegro Przesyłka polecona',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '6.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'2b6ca59d-1e4c-426c-82a9-efcbd730846b' =>
		array (
			'id' => '2b6ca59d-1e4c-426c-82a9-efcbd730846b',
			'name' => 'Paczka pocztowa ekonomiczna',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT72H',
								),
							'customizable' => true,
						),
				),
		),
	'7203cb90-864c-4cda-bf08-dc883f0c78ad' =>
		array (
			'id' => '7203cb90-864c-4cda-bf08-dc883f0c78ad',
			'name' => 'Przesyłka kurierska',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => true,
						),
				),
		),
	'845efe05-0c96-47c3-a8cb-aa4699c158ce' =>
		array (
			'id' => '845efe05-0c96-47c3-a8cb-aa4699c158ce',
			'name' => 'Przesyłka kurierska pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => true,
						),
				),
		),
	'afa68f62-0154-4556-9ee4-3f97c3c5bdca' =>
		array (
			'id' => 'afa68f62-0154-4556-9ee4-3f97c3c5bdca',
			'name' => 'Odbiór osobisty w punkcie sprzedawcy',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 1,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => true,
						),
				),
		),
	'b6d6e4cf-67b3-4a4f-a38d-724335e9734f' =>
		array (
			'id' => 'b6d6e4cf-67b3-4a4f-a38d-724335e9734f',
			'name' => 'Odbiór osobisty w punkcie sprzedawcy pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 1,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => true,
						),
				),
		),
	'b715fac1-8ec2-4f5c-8fdf-0f9cec9085ad' =>
		array (
			'id' => 'b715fac1-8ec2-4f5c-8fdf-0f9cec9085ad',
			'name' => 'ORLEN Paczka',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'121981e4-a14b-450b-b9fe-718df1dc3cd1' =>
		array (
			'id' => '121981e4-a14b-450b-b9fe-718df1dc3cd1',
			'name' => 'ORLEN Paczka pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'5d9c7838-e05f-4dec-afdd-58e884170ba7' =>
		array (
			'id' => '5d9c7838-e05f-4dec-afdd-58e884170ba7',
			'name' => 'Allegro Kurier24 InPost',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '13.41',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'85c3ad2f-4ec1-446c-866e-63473ed10e26' =>
		array (
			'id' => '85c3ad2f-4ec1-446c-866e-63473ed10e26',
			'name' => 'Allegro Kurier24 InPost pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '15.87',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'ef201386-9a7a-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'ef201386-9a7a-11e5-8994-feff819cdc9f',
			'name' => 'Austria',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'd961752c-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd961752c-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Białoruś',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'd9617d4c-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9617d4c-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Belgia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0160-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0160-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Bułgaria',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT120H',
									'to' => 'PT144H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0164-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0164-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Cypr',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT120H',
									'to' => 'PT168H',
								),
							'customizable' => true,
						),
				),
		),
	'd9616b72-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9616b72-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Czechy',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'd9616816-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9616816-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Niemcy',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'd96175f4-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd96175f4-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Norwegia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'd9616e1a-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9616e1a-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Irlandia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'd9616f0a-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9616f0a-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Rosja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'd9617928-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9617928-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Ukraina',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0156-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0156-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Estonia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0158-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0158-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Łotwa',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'd9616a6e-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9616a6e-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Wielka Brytania',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'd96179f0-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd96179f0-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Włochy',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'd9617ee6-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9617ee6-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Dania',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'd961709a-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd961709a-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Szwecja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'd9617388-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9617388-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Holandia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'ef2005d0-9a7a-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'ef2005d0-9a7a-11e5-8994-feff819cdc9f',
			'name' => 'Hiszpania',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT120H',
									'to' => 'PT144H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0166-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0166-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Finlandia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0162-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0162-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Chorwacja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT120H',
									'to' => 'PT144H',
								),
							'customizable' => true,
						),
				),
		),
	'd9616fd2-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9616fd2-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Słowacja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'd9617464-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9617464-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Litwa',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'd9617aae-9a79-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'd9617aae-9a79-11e5-8994-feff819cdc9f',
			'name' => 'Francja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0174-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0174-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Portugalia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT120H',
									'to' => 'PT144H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0172-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0172-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Malta',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT120H',
									'to' => 'PT168H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0170-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0170-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Luksemburg',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0178-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0178-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Słowenia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0168-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0168-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Grecja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT120H',
									'to' => 'PT144H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0176-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0176-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Rumunia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT120H',
								),
							'customizable' => true,
						),
				),
		),
	'b12a0180-9a7b-11e5-8994-feff819cdc9f' =>
		array (
			'id' => 'b12a0180-9a7b-11e5-8994-feff819cdc9f',
			'name' => 'Węgry',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'10b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '10b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Austria',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '29.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'11b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '11b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Belgia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '50.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'12b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '12b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Bułgaria',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '71.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'13b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '13b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Chorwacja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '75.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'14b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '14b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Czechy',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '15.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'15b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '15b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Dania',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '72.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'16b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '16b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Estonia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '51.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'17b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '17b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Finlandia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '94.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'18b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '18b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Francja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '70.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'19b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '19b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Grecja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '113.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT120H',
									'to' => 'PT120H',
								),
							'customizable' => false,
						),
				),
		),
	'20b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '20b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Hiszpania',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '86.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'21b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '21b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Holandia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '27.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'22b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '22b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Irlandia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '75.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'23b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '23b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Litwa',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '17.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'24b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '24b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Luksemburg',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '50.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'25b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '25b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Łotwa',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '38.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'26b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '26b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Niemcy',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '13.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'27b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '27b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Portugalia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '94.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'28b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '28b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Rumunia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '32.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'29b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '29b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Słowacja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '15.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'30b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '30b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Słowenia',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '19.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'31b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '31b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Szwecja',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '90.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT96H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'32b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '32b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Węgry',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '12.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'34b73cc6-28d6-11eb-adc1-0242ac120002' =>
		array (
			'id' => '34b73cc6-28d6-11eb-adc1-0242ac120002',
			'name' => 'Allegro Kurier DPD Włochy',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '73.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => false,
						),
				),
		),
	'45309171-0415-49cd-b2cf-89e9143d20f0' =>
		array (
			'id' => '45309171-0415-49cd-b2cf-89e9143d20f0',
			'name' => 'Paczka pocztowa priorytetowa',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => true,
						),
				),
		),
	'758fcd59-fbfa-4453-ae07-4800d72c2ca5' =>
		array (
			'id' => '758fcd59-fbfa-4453-ae07-4800d72c2ca5',
			'name' => 'List polecony priorytetowy',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT72H',
								),
							'customizable' => true,
						),
				),
		),
	'2488f7b7-5d1c-4d65-b85c-4cbcf253fd93' =>
		array (
			'id' => '2488f7b7-5d1c-4d65-b85c-4cbcf253fd93',
			'name' => 'Allegro Paczkomaty InPost',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '8.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'b20ef9e1-faa2-4f25-9032-adbea23e5cb9' =>
		array (
			'id' => 'b20ef9e1-faa2-4f25-9032-adbea23e5cb9',
			'name' => 'Allegro Paczkomaty InPost pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '12.49',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'7b45d0c7-5e22-46ac-824e-f0384e9aab52' =>
		array (
			'id' => '7b45d0c7-5e22-46ac-824e-f0384e9aab52',
			'name' => 'Paczka24 Odbiór w Punkcie',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'b9e02dd2-6e91-4d13-901c-5e56ab0d1cd7' =>
		array (
			'id' => 'b9e02dd2-6e91-4d13-901c-5e56ab0d1cd7',
			'name' => 'Paczka24 Odbiór w Punkcie pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'1fa56f79-4b6a-4821-a6f2-ca9c16d5c922' =>
		array (
			'id' => '1fa56f79-4b6a-4821-a6f2-ca9c16d5c922',
			'name' => 'List ekonomiczny',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'bc2a5eb6-921f-4c1e-ab96-3a1b747ff9f6' =>
		array (
			'id' => 'bc2a5eb6-921f-4c1e-ab96-3a1b747ff9f6',
			'name' => 'List priorytetowy',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'773167b1-feec-4ae9-b20f-1ed8ccb7b1ed' =>
		array (
			'id' => '773167b1-feec-4ae9-b20f-1ed8ccb7b1ed',
			'name' => 'List polecony ekonomiczny',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT72H',
									'to' => 'PT96H',
								),
							'customizable' => true,
						),
				),
		),
	'74bc07eb-552f-4581-b68c-da46716d4a9a' =>
		array (
			'id' => '74bc07eb-552f-4581-b68c-da46716d4a9a',
			'name' => 'Paczka24',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'b90c6295-b69a-4cb4-a308-7126a02aea47' =>
		array (
			'id' => 'b90c6295-b69a-4cb4-a308-7126a02aea47',
			'name' => 'Paczka24 pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'd1aa489b-baf3-4f9d-a96e-2c4e497dc7e5' =>
		array (
			'id' => 'd1aa489b-baf3-4f9d-a96e-2c4e497dc7e5',
			'name' => 'Przesyłka elektroniczna (e-mail)',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 1,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'd44995ed-edd2-4abb-999f-a56bd0f12a68' =>
		array (
			'id' => 'd44995ed-edd2-4abb-999f-a56bd0f12a68',
			'name' => 'Punkty Poczta, Żabka, Orlen, Ruch',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'e36c9ccf-e341-49a9-8fa0-f073dcd61210' =>
		array (
			'id' => 'e36c9ccf-e341-49a9-8fa0-f073dcd61210',
			'name' => 'Punkty Poczta pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'685d8b40-2571-4111-8937-9220b1710d4c' =>
		array (
			'id' => '685d8b40-2571-4111-8937-9220b1710d4c',
			'name' => 'Paczkomaty InPost',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'2653ca13-67c8-48c3-bbf8-ff9aa3f70ed3' =>
		array (
			'id' => '2653ca13-67c8-48c3-bbf8-ff9aa3f70ed3',
			'name' => 'Paczkomaty InPost pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'f7e952b5-9ae8-40a9-90dd-e71ab9da29dd' =>
		array (
			'id' => 'f7e952b5-9ae8-40a9-90dd-e71ab9da29dd',
			'name' => 'Pocztex Kurier48',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'8cc6e982-0af4-4e12-be96-5387db79a166' =>
		array (
			'id' => '8cc6e982-0af4-4e12-be96-5387db79a166',
			'name' => 'Pocztex Kurier48 pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'9081532b-5ad3-467d-80bc-9252982e9dd8' =>
		array (
			'id' => '9081532b-5ad3-467d-80bc-9252982e9dd8',
			'name' => 'Allegro miniKurier24 InPost',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '10.95',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'98f86f81-0018-41c5-ac83-073a56fc7021' =>
		array (
			'id' => '98f86f81-0018-41c5-ac83-073a56fc7021',
			'name' => 'Allegro miniKurier24 InPost pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '13.41',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'0d69a62b-7a66-4bd1-9134-49dea68dccc8' =>
		array (
			'id' => '0d69a62b-7a66-4bd1-9134-49dea68dccc8',
			'name' => 'Allegro Odbiór w Punkcie UPS',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '8.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'08e2ef8e-90c8-49db-8970-d6c2773f1530' =>
		array (
			'id' => '08e2ef8e-90c8-49db-8970-d6c2773f1530',
			'name' => 'Allegro Odbiór w Punkcie DPD Pickup',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => true,
							'min' => '5.000',
							'max' => '700.000',
							'unit' => 'KILOGRAM',
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '8.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'1d037bcd-7e6a-4713-9015-d6c2773f1530' =>
		array (
			'id' => '1d037bcd-7e6a-4713-9015-d6c2773f1530',
			'name' => 'Allegro Odbiór w Punkcie DPD Pickup pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => true,
							'min' => '5.000',
							'max' => '700.000',
							'unit' => 'KILOGRAM',
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '13.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'0aafb43c-e66a-46ec-9cc4-29bb39ebb483' =>
		array (
			'id' => '0aafb43c-e66a-46ec-9cc4-29bb39ebb483',
			'name' => 'Allegro One Box',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '7.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'0b9bed2c-0bc1-4e1f-9694-29bb39ebb483' =>
		array (
			'id' => '0b9bed2c-0bc1-4e1f-9694-29bb39ebb483',
			'name' => 'Allegro One Box, One Kurier',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '7.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'0e454c54-21dd-4e5c-9db2-0553724f9077' =>
		array (
			'id' => '0e454c54-21dd-4e5c-9db2-0553724f9077',
			'name' => 'Allegro One Box, One Kurier - dostawa dzisiaj',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '10.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'0ffc08e7-9ef9-4189-adcc-ec6d99f27f02' =>
		array (
			'id' => '0ffc08e7-9ef9-4189-adcc-ec6d99f27f02',
			'name' => 'Allegro One Kurier - dostawa dzisiaj (do 20.09.2022)',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '14.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'0decb356-e694-42e7-82eb-f260691b856f' =>
		array (
			'id' => '0decb356-e694-42e7-82eb-f260691b856f',
			'name' => 'Allegro One Kurier - dostawa dzisiaj',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '14.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'1fa56f79-4b6a-4821-a6f2-ca9c16d5c925' =>
		array (
			'id' => '1fa56f79-4b6a-4821-a6f2-ca9c16d5c925',
			'name' => 'Dostawa i jej koszt ustalane ze sprzedającym',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
				),
		),
	'0ce12f52-e6aa-4ce5-a951-f54c1643bcb5' =>
		array (
			'id' => '0ce12f52-e6aa-4ce5-a951-f54c1643bcb5',
			'name' => 'Allegro DHL Palety',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '300.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '150.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'1655e292-7dd1-4d2f-b68b-f54c1643bcb5' =>
		array (
			'id' => '1655e292-7dd1-4d2f-b68b-f54c1643bcb5',
			'name' => 'Allegro DHL Palety pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '300.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '150.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'240f88db-7a7a-4d27-8004-9afbf05cfcd1' =>
		array (
			'id' => '240f88db-7a7a-4d27-8004-9afbf05cfcd1',
			'name' => 'Kurier Geis',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'3a9b10ce-abd5-4537-be33-9afbf05cfcd1' =>
		array (
			'id' => '3a9b10ce-abd5-4537-be33-9afbf05cfcd1',
			'name' => 'Kurier Geis pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'8cf74350-94e8-4be7-ba3d-a736dd7621ec' =>
		array (
			'id' => '8cf74350-94e8-4be7-ba3d-a736dd7621ec',
			'name' => 'Allegro Zadbano',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'9b8e4329-92fe-479c-93de-778d33b2ff65' =>
		array (
			'id' => '9b8e4329-92fe-479c-93de-778d33b2ff65',
			'name' => 'Allegro Zadbano pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT72H',
								),
							'customizable' => false,
						),
				),
		),
	'00bc935e-b423-4cd4-9849-5760758db049' =>
		array (
			'id' => '00bc935e-b423-4cd4-9849-5760758db049',
			'name' => 'Kurier Ambro Express',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'1fe8c93d-3a78-426d-8b91-5760758db049' =>
		array (
			'id' => '1fe8c93d-3a78-426d-8b91-5760758db049',
			'name' => 'Kurier Ambro Express pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'8098f3dd-6d2f-4cdb-b317-9a2f986f2ec3' =>
		array (
			'id' => '8098f3dd-6d2f-4cdb-b317-9a2f986f2ec3',
			'name' => 'Allegro One Kurier - dostawa jutro (do 01.09.2022)',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '12.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'01d3468e-42af-474d-b06c-9a2f986f2ec3' =>
		array (
			'id' => '01d3468e-42af-474d-b06c-9a2f986f2ec3',
			'name' => 'Allegro One Kurier - dostawa jutro (do 20.09.2022)',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '10.90',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'0d83955a-4297-4169-adef-9790d53b11ff' =>
		array (
			'id' => '0d83955a-4297-4169-adef-9790d53b11ff',
			'name' => 'Allegro One Kurier - dostawa jutro',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '10.90',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'ca225f8e-f5c5-42e9-a234-49fb629cd912' =>
		array (
			'id' => 'ca225f8e-f5c5-42e9-a234-49fb629cd912',
			'name' => 'Kurier Rhenus',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'ec6f7e1f-dda2-4ef3-bd34-deac136e0b4b' =>
		array (
			'id' => 'ec6f7e1f-dda2-4ef3-bd34-deac136e0b4b',
			'name' => 'Kurier Rhenus pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT48H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'42ea7239-28f2-4a0e-90da-e3934646bc7d' =>
		array (
			'id' => '42ea7239-28f2-4a0e-90da-e3934646bc7d',
			'name' => 'Kurier Patron Service',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'5f439306-af7c-4371-9c88-e3934646bc7d' =>
		array (
			'id' => '5f439306-af7c-4371-9c88-e3934646bc7d',
			'name' => 'Kurier Patron Service pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'c3066682-97a3-42fe-9eb5-3beeccab840c' =>
		array (
			'id' => 'c3066682-97a3-42fe-9eb5-3beeccab840c',
			'name' => 'Allegro Kurier DPD',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => true,
							'min' => '5.000',
							'max' => '700.000',
							'unit' => 'KILOGRAM',
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '12.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'259b5c7a-9056-4c74-80ec-8bdc50cb0413' =>
		array (
			'id' => '259b5c7a-9056-4c74-80ec-8bdc50cb0413',
			'name' => 'Allegro Kurier DPD pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => true,
							'min' => '5.000',
							'max' => '700.000',
							'unit' => 'KILOGRAM',
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '17.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'c9091ffb-727e-4a88-bd7c-b17dddbefb4a' =>
		array (
			'id' => 'c9091ffb-727e-4a88-bd7c-b17dddbefb4a',
			'name' => 'Dostawa przez sprzedającego',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => true,
						),
				),
		),
	'd1744bea-fb26-49d4-a2b9-2b59c25132e1' =>
		array (
			'id' => 'd1744bea-fb26-49d4-a2b9-2b59c25132e1',
			'name' => 'Dostawa przez sprzedającego pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => true,
						),
				),
		),
	'c576dc1d-4046-4b53-94de-47fc5b6913d4' =>
		array (
			'id' => 'c576dc1d-4046-4b53-94de-47fc5b6913d4',
			'name' => 'Odbiór w Punkcie UPS',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'e6d9d675-c8cb-4c0f-bcd2-febebfbac3d5' =>
		array (
			'id' => 'e6d9d675-c8cb-4c0f-bcd2-febebfbac3d5',
			'name' => 'Odbiór w Punkcie UPS pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'07d4d067-2e02-4011-aa6e-491f6e8fe266' =>
		array (
			'id' => '07d4d067-2e02-4011-aa6e-491f6e8fe266',
			'name' => 'Odbiór w Punkcie Pocztex',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'146b2c5d-d2f1-43b7-8268-491f6e8fe266' =>
		array (
			'id' => '146b2c5d-d2f1-43b7-8268-491f6e8fe266',
			'name' => 'Odbiór w Punkcie Pocztex pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'02193188-23db-4017-8a22-6f93ea07ca13' =>
		array (
			'id' => '02193188-23db-4017-8a22-6f93ea07ca13',
			'name' => 'Automat Pocztex',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'0175efcd-8291-4328-ac41-258f5807b44e' =>
		array (
			'id' => '0175efcd-8291-4328-ac41-258f5807b44e',
			'name' => 'Kurier Pocztex',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'1175efcd-15f3-4e97-8b15-258f5807b44e' =>
		array (
			'id' => '1175efcd-15f3-4e97-8b15-258f5807b44e',
			'name' => 'Kurier Pocztex pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'093d950b-8497-4cc7-aa2a-6081e5e7e7e2' =>
		array (
			'id' => '093d950b-8497-4cc7-aa2a-6081e5e7e7e2',
			'name' => 'Allegro Kurier Pocztex',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '11.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'193d950b-0082-408f-9192-6081e5e7e7e2' =>
		array (
			'id' => '193d950b-0082-408f-9192-6081e5e7e7e2',
			'name' => 'Allegro Kurier Pocztex pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '16.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'028e2be0-1c06-41cf-a309-ec5557273786' =>
		array (
			'id' => '028e2be0-1c06-41cf-a309-ec5557273786',
			'name' => 'Allegro Odbiór w Punkcie Pocztex',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '8.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'128e2be0-a2b9-4793-aafd-ec5557273786' =>
		array (
			'id' => '128e2be0-a2b9-4793-aafd-ec5557273786',
			'name' => 'Allegro Odbiór w Punkcie Pocztex pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '13.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'0fc30a71-a1c3-4572-8217-c29515a5f0d1' =>
		array (
			'id' => '0fc30a71-a1c3-4572-8217-c29515a5f0d1',
			'name' => 'Allegro Automat Pocztex',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '8.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'059c0d58-6cdb-4955-ab79-9031518f80f3' =>
		array (
			'id' => '059c0d58-6cdb-4955-ab79-9031518f80f3',
			'name' => 'Kurier GLS',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'1205bb4a-7d74-46a4-b7de-1cb66a3d0c03' =>
		array (
			'id' => '1205bb4a-7d74-46a4-b7de-1cb66a3d0c03',
			'name' => 'Kurier GLS pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'574d1c9e-9903-4626-903f-f72441d520d5' =>
		array (
			'id' => '574d1c9e-9903-4626-903f-f72441d520d5',
			'name' => 'Allegro ORLEN Paczka',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '8.49',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'2503b262-e08c-4c2b-8598-38e0603d0d70' =>
		array (
			'id' => '2503b262-e08c-4c2b-8598-38e0603d0d70',
			'name' => 'Allegro ORLEN Paczka pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '11.06',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'6eced786-aee4-47ba-9cb6-e24f8b1d3182' =>
		array (
			'id' => '6eced786-aee4-47ba-9cb6-e24f8b1d3182',
			'name' => 'Allegro Pocztex Kurier 48',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '11.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'ec0e7fff-8233-4869-99ce-fe2b67a7de84' =>
		array (
			'id' => 'ec0e7fff-8233-4869-99ce-fe2b67a7de84',
			'name' => 'Allegro Pocztex Kurier 48 pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '16.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'881504b1-13e5-4280-b453-f1211164778e' =>
		array (
			'id' => '881504b1-13e5-4280-b453-f1211164778e',
			'name' => 'Kurier FedEx',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'ba32c220-71e1-4df6-ab2b-7d5ff19a952d' =>
		array (
			'id' => 'ba32c220-71e1-4df6-ab2b-7d5ff19a952d',
			'name' => 'Kurier FedEx pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'6e68e08c-23d2-48f3-8d7a-c7e93bf55853' =>
		array (
			'id' => '6e68e08c-23d2-48f3-8d7a-c7e93bf55853',
			'name' => 'Kurier Pekaes',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'7be240c9-d718-4c0d-bfcc-c7e93bf55853' =>
		array (
			'id' => '7be240c9-d718-4c0d-bfcc-c7e93bf55853',
			'name' => 'Kurier Pekaes pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'07354b22-6484-4e90-b230-6a9dd91d95b2' =>
		array (
			'id' => '07354b22-6484-4e90-b230-6a9dd91d95b2',
			'name' => 'Kurier DB Schenker',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'142f3a14-83c9-4aa0-8580-6a9dd91d95b2' =>
		array (
			'id' => '142f3a14-83c9-4aa0-8580-6a9dd91d95b2',
			'name' => 'Kurier DB Schenker pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'1a228763-c17a-4b2d-88b0-63b082b04da6' =>
		array (
			'id' => '1a228763-c17a-4b2d-88b0-63b082b04da6',
			'name' => 'Kurier InPost',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'999f8753-6340-48a0-8eba-46096f9749aa' =>
		array (
			'id' => '999f8753-6340-48a0-8eba-46096f9749aa',
			'name' => 'Kurier InPost pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'e4d44e17-d107-447b-bebf-d82c636fe739' =>
		array (
			'id' => 'e4d44e17-d107-447b-bebf-d82c636fe739',
			'name' => 'Kurier Raben',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'25d687d5-5aab-41ef-afb6-c12ac1603c17' =>
		array (
			'id' => '25d687d5-5aab-41ef-afb6-c12ac1603c17',
			'name' => 'Kurier Raben pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'c8be0338-63f9-42d8-bc78-ee2bd45980b6' =>
		array (
			'id' => 'c8be0338-63f9-42d8-bc78-ee2bd45980b6',
			'name' => 'Kurier DHL',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'8de8c002-9f06-4e91-b8bc-aebcae3b9fe7' =>
		array (
			'id' => '8de8c002-9f06-4e91-b8bc-aebcae3b9fe7',
			'name' => 'Kurier DHL pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'849a8308-240d-4045-ab30-f8207815bce4' =>
		array (
			'id' => '849a8308-240d-4045-ab30-f8207815bce4',
			'name' => 'Allegro Punkty Poczta, Żabka, Orlen, Ruch',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '8.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'88f677d9-c125-45e0-bdf0-f965826542a9' =>
		array (
			'id' => '88f677d9-c125-45e0-bdf0-f965826542a9',
			'name' => 'Allegro Punkty Poczta pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '13.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'84d052b8-e148-4f15-9581-93820784def1' =>
		array (
			'id' => '84d052b8-e148-4f15-9581-93820784def1',
			'name' => 'Kurier TNT Express',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'954bc216-f562-48de-8e8e-93820784def1' =>
		array (
			'id' => '954bc216-f562-48de-8e8e-93820784def1',
			'name' => 'Kurier TNT Express pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'04b07522-70e5-4dc3-b13b-7918ea717539' =>
		array (
			'id' => '04b07522-70e5-4dc3-b13b-7918ea717539',
			'name' => 'Allegro One Kurier - dostawa dzisiaj (do 01.09.2022)',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '14.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'17535ba6-077b-4371-ab4c-7918ea717539' =>
		array (
			'id' => '17535ba6-077b-4371-ab4c-7918ea717539',
			'name' => 'Allegro One Kurier pobranie - dostawa dzisiaj (do 01.09.2022)',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => false,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '18.44',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'b4525da7-fe10-4e89-af86-70479d3f251e' =>
		array (
			'id' => 'b4525da7-fe10-4e89-af86-70479d3f251e',
			'name' => 'Kurier UPS',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'496c5d07-a095-4cf3-bc3a-2217e01172a3' =>
		array (
			'id' => '496c5d07-a095-4cf3-bc3a-2217e01172a3',
			'name' => 'Kurier UPS pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'c3066682-97a3-42fe-9eb5-9bb2978cf293' =>
		array (
			'id' => 'c3066682-97a3-42fe-9eb5-9bb2978cf293',
			'name' => 'Kurier DPD',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'259b5c7a-9056-4c74-80ec-9bb2978cf293' =>
		array (
			'id' => '259b5c7a-9056-4c74-80ec-9bb2978cf293',
			'name' => 'Kurier DPD pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'0ee3467f-5451-4060-856e-7a2b502abe55' =>
		array (
			'id' => '0ee3467f-5451-4060-856e-7a2b502abe55',
			'name' => 'Allegro One Punkt, One Kurier',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '8.49',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'08c15d0b-9b24-4b3f-89a8-7a2b502abe55' =>
		array (
			'id' => '08c15d0b-9b24-4b3f-89a8-7a2b502abe55',
			'name' => 'Allegro One Punkt',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '8.49',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'0e4c7d59-64b6-4b06-89c3-c1d941506dd0' =>
		array (
			'id' => '0e4c7d59-64b6-4b06-89c3-c1d941506dd0',
			'name' => 'Allegro Kurier UPS',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => true,
							'min' => '5.000',
							'max' => '700.000',
							'unit' => 'KILOGRAM',
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '12.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'199d2a2a-7c90-4ca7-aaf3-c1d941506dd0' =>
		array (
			'id' => '199d2a2a-7c90-4ca7-aaf3-c1d941506dd0',
			'name' => 'Allegro Kurier UPS pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => true,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => true,
							'min' => '5.000',
							'max' => '700.000',
							'unit' => 'KILOGRAM',
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '17.99',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT24H',
								),
							'customizable' => false,
						),
				),
		),
	'41cc412c-ba9e-422d-9750-7918ea717539' =>
		array (
			'id' => '41cc412c-ba9e-422d-9750-7918ea717539',
			'name' => 'Kurier Pickpack',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '100000000.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT0S',
									'to' => 'PT0S',
								),
							'customizable' => false,
						),
				),
		),
	'a129b958-356f-11eb-adc1-0242ac120002' =>
		array (
			'id' => 'a129b958-356f-11eb-adc1-0242ac120002',
			'name' => 'Allegro No Limit',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '400.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT120H',
								),
							'customizable' => false,
						),
				),
		),
	'2e3782e0-3579-11eb-adc1-0242ac120002' =>
		array (
			'id' => '2e3782e0-3579-11eb-adc1-0242ac120002',
			'name' => 'Allegro No Limit pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '400.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT120H',
								),
							'customizable' => false,
						),
				),
		),
	'ae814e65-d1e8-463a-85b2-4dff7e5146a7' =>
		array (
			'id' => 'ae814e65-d1e8-463a-85b2-4dff7e5146a7',
			'name' => 'Allegro SUUS Logistics',
			'paymentPolicy' => 'IN_ADVANCE',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '300.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
	'b7d7cc1b-04c9-4a47-be31-4dff7e5146a7' =>
		array (
			'id' => 'b7d7cc1b-04c9-4a47-be31-4dff7e5146a7',
			'name' => 'Allegro SUUS Logistics pobranie',
			'paymentPolicy' => 'CASH_ON_DELIVERY',
			'allegroEndorsed' => false,
			'shippingRatesConstraints' =>
				array (
					'allowed' => true,
					'maxQuantityPerPackage' =>
						array (
							'max' => 999999,
						),
					'maxPackageWeight' =>
						array (
							'supported' => false,
						),
					'firstItemRate' =>
						array (
							'min' => '0.00',
							'max' => '300.00',
							'currency' => 'PLN',
						),
					'nextItemRate' =>
						array (
							'min' => '0.00',
							'max' => '0.00',
							'currency' => 'PLN',
						),
					'shippingTime' =>
						array (
							'default' =>
								array (
									'from' => 'PT24H',
									'to' => 'PT48H',
								),
							'customizable' => false,
						),
				),
		),
);

$delivery_methods = [];
foreach ( $data['deliveryMethodsGroups'] as $group ) {
	if ( in_array( $group['name'], [ 'Wysyłka z zagranicy do Polski', 'Wysyłka elektroniczna', 'Wysyłka za granicę' ] )) {
		continue;
	}
//print_r( $group['name'] );
	foreach ( $group['deliveryMethodsSubgroups'] as $subgroup ) {
//		print_r( $subgroup );
		foreach ( $subgroup['deliveryMethods'] as $method ) {
			$delivery_methods[ $method['id'] ] = [
				'name' => $method['name'],
				'group'=> $group['name'],
				'subgroup' => $subgroup['name'],
			];
//			print_r( $methods );
		}
	}
}

var_export( $delivery_methods );
