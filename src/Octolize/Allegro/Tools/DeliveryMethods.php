<?php


use Octolize\Allegro\Shipping\Exceptions\UnknownDeliveryMethod;

class DeliveryMethods {

	const INPOST = 'inpost';
	const DPD = 'dpd';
	const UPS = 'ups';
	const POCZTA_POLSKA = 'poczta-polska';
	const ALLEGRO_ONE = 'allegro-one';
	const ORLEN_PACZKA = 'orlen-paczka';
	const X_PRESS = 'x-press';
	const ZADBANO = 'zadbano';
	const SUUS = 'suus';
	const NO_LIMIT = 'no-limit';
	const DHL = 'dhl';
	const AMBRO = 'ambro';
	const GLS = 'gls';
	const FEDEX = 'fedex';
	const PICKPACK = 'pickpack';
	const RABEN = 'raben';
	const RHENUS = 'rhenus';
	const DB_SCHENKER = 'db-schenker';
	const GEIS = 'geis';
	const PATRON_SERVICE = 'patron-service';
	const PEKAES = 'pekaes';
	const TNT_EXPRESS = 'tnt-express';
	const OTHER = 'other';

	const DELIVERY_METHODS = [
			'2488f7b7-5d1c-4d65-b85c-4cbcf253fd93' =>
				[
					'name'     => 'Allegro Paczkomaty InPost',
					'group'    => 'Oferta specjalna Allegro - Inpost',
					'subgroup' => 'Allegro Paczkomaty InPost',
					'provider' => self::INPOST,
				],
			'b20ef9e1-faa2-4f25-9032-adbea23e5cb9' =>
				[
					'name'     => 'Allegro Paczkomaty InPost pobranie',
					'group'    => 'Oferta specjalna Allegro - Inpost',
					'subgroup' => 'Allegro Paczkomaty InPost',
					'provider' => self::INPOST,
				],
			'9081532b-5ad3-467d-80bc-9252982e9dd8' =>
				[
					'name'     => 'Allegro miniKurier24 InPost',
					'group'    => 'Oferta specjalna Allegro - Inpost',
					'subgroup' => 'Allegro miniKurier24 InPost',
					'provider' => self::INPOST,
				],
			'98f86f81-0018-41c5-ac83-073a56fc7021' =>
				[
					'name'     => 'Allegro miniKurier24 InPost pobranie',
					'group'    => 'Oferta specjalna Allegro - Inpost',
					'subgroup' => 'Allegro miniKurier24 InPost',
					'provider' => self::INPOST,
				],
			'5d9c7838-e05f-4dec-afdd-58e884170ba7' =>
				[
					'name'     => 'Allegro Kurier24 InPost',
					'group'    => 'Oferta specjalna Allegro - Inpost',
					'subgroup' => 'Allegro Kurier24 InPost',
					'provider' => self::INPOST,
				],
			'85c3ad2f-4ec1-446c-866e-63473ed10e26' =>
				[
					'name'     => 'Allegro Kurier24 InPost pobranie',
					'group'    => 'Oferta specjalna Allegro - Inpost',
					'subgroup' => 'Allegro Kurier24 InPost',
					'provider' => self::INPOST,
				],
			'08c15d0b-9b24-4b3f-89a8-7a2b502abe55' =>
				[
					'name'     => 'Allegro One Punkt',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro One (Punkt, Box)',
					'provider' => self::ALLEGRO_ONE,
				],
			'0aafb43c-e66a-46ec-9cc4-29bb39ebb483' =>
				[
					'name'     => 'Allegro One Box',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro One (Punkt, Box)',
					'provider' => self::ALLEGRO_ONE,
				],
			'0e454c54-21dd-4e5c-9db2-0553724f9077' =>
				[
					'name'     => 'Allegro One Box, One Kurier - dostawa dzisiaj',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro One (Punkt, Box, Kurier)',
					'provider' => self::ALLEGRO_ONE,
				],
			'0decb356-e694-42e7-82eb-f260691b856f' =>
				[
					'name'     => 'Allegro One Kurier - dostawa dzisiaj',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro One (Punkt, Box, Kurier)',
					'provider' => self::ALLEGRO_ONE,
				],
			'0ee3467f-5451-4060-856e-7a2b502abe55' =>
				[
					'name'     => 'Allegro One Punkt, One Kurier',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro One (Punkt, Box, Kurier)',
					'provider' => self::ALLEGRO_ONE,
				],
			'0b9bed2c-0bc1-4e1f-9694-29bb39ebb483' =>
				[
					'name'     => 'Allegro One Box, One Kurier',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro One (Punkt, Box, Kurier)',
					'provider' => self::ALLEGRO_ONE,
				],
			'0d83955a-4297-4169-adef-9790d53b11ff' =>
				[
					'name'     => 'Allegro One Kurier - dostawa jutro',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro One (Punkt, Box, Kurier)',
					'provider' => self::ALLEGRO_ONE,
				],
			'0ffc08e7-9ef9-4189-adcc-ec6d99f27f02' =>
				[
					'name'     => 'Allegro One Kurier - dostawa dzisiaj (do 20.09.2022)',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro One Kurier - dostawa dzisiaj (do 20.09.2022)',
					'provider' => self::ALLEGRO_ONE,
				],
			'01d3468e-42af-474d-b06c-9a2f986f2ec3' =>
				[
					'name'     => 'Allegro One Kurier - dostawa jutro (do 20.09.2022)',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro One Kurier - dostawa jutro (do 20.09.2022)',
					'provider' => self::ALLEGRO_ONE,
				],
			'c3066682-97a3-42fe-9eb5-3beeccab840c' =>
				[
					'name'     => 'Allegro Kurier DPD',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'259b5c7a-9056-4c74-80ec-8bdc50cb0413' =>
				[
					'name'     => 'Allegro Kurier DPD pobranie',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'08e2ef8e-90c8-49db-8970-d6c2773f1530' =>
				[
					'name'     => 'Allegro Odbiór w Punkcie DPD Pickup',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Odbiór w Punkcie DPD Pickup',
					'provider' => self::DPD,
				],
			'1d037bcd-7e6a-4713-9015-d6c2773f1530' =>
				[
					'name'     => 'Allegro Odbiór w Punkcie DPD Pickup pobranie',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Odbiór w Punkcie DPD Pickup',
					'provider' => self::DPD,
				],
			'0e4c7d59-64b6-4b06-89c3-c1d941506dd0' =>
				[
					'name'     => 'Allegro Kurier UPS',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Kurier UPS',
					'provider' => self::UPS,
				],
			'199d2a2a-7c90-4ca7-aaf3-c1d941506dd0' =>
				[
					'name'     => 'Allegro Kurier UPS pobranie',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Kurier UPS',
					'provider' => self::UPS,
				],
			'0d69a62b-7a66-4bd1-9134-49dea68dccc8' =>
				[
					'name'     => 'Allegro Odbiór w Punkcie UPS',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Odbiór w Punkcie UPS',
					'provider' => self::UPS,
				],
			'093d950b-8497-4cc7-aa2a-6081e5e7e7e2' =>
				[
					'name'     => 'Allegro Kurier Pocztex',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Kurier Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'193d950b-0082-408f-9192-6081e5e7e7e2' =>
				[
					'name'     => 'Allegro Kurier Pocztex pobranie',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Kurier Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'028e2be0-1c06-41cf-a309-ec5557273786' =>
				[
					'name'     => 'Allegro Odbiór w Punkcie Pocztex',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Odbiór w Punkcie Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'128e2be0-a2b9-4793-aafd-ec5557273786' =>
				[
					'name'     => 'Allegro Odbiór w Punkcie Pocztex pobranie',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Odbiór w Punkcie Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'0fc30a71-a1c3-4572-8217-c29515a5f0d1' =>
				[
					'name'     => 'Allegro Automat Pocztex',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Automat Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'6eced786-aee4-47ba-9cb6-e24f8b1d3182' =>
				[
					'name'     => 'Allegro Pocztex Kurier 48',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Pocztex Kurier 48',
					'provider' => self::POCZTA_POLSKA,
				],
			'ec0e7fff-8233-4869-99ce-fe2b67a7de84' =>
				[
					'name'     => 'Allegro Pocztex Kurier 48 pobranie',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Pocztex Kurier 48',
					'provider' => self::POCZTA_POLSKA,
				],
			'849a8308-240d-4045-ab30-f8207815bce4' =>
				[
					'name'     => 'Allegro Punkty Poczta, Żabka, Orlen, Ruch',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Punkty Poczta, Żabka, Orlen, Ruch',
					'provider' => self::POCZTA_POLSKA,
				],
			'88f677d9-c125-45e0-bdf0-f965826542a9' =>
				[
					'name'     => 'Allegro Punkty Poczta pobranie',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Punkty Poczta, Żabka, Orlen, Ruch',
					'provider' => self::POCZTA_POLSKA,
				],
			'aa1d05e0-943b-47cb-a759-9d8c16707129' =>
				[
					'name'     => 'Allegro Przesyłka polecona',
					'group'    => 'Sposób rozliczeń Allegro Standard',
					'subgroup' => 'Allegro Przesyłka polecona',
					'provider' => self::POCZTA_POLSKA,
				],
			'10b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Austria',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'11b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Belgia',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'12b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Bułgaria',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'13b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Chorwacja',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'14b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Czechy',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'15b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Dania',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'16b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Estonia',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'17b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Finlandia',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'18b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Francja',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'19b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Grecja',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'20b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Hiszpania',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'21b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Holandia',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'22b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Irlandia',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'23b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Litwa',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'24b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Luksemburg',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'25b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Łotwa',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'26b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Niemcy',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'27b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Portugalia',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'28b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Rumunia',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'29b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Słowacja',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'30b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Słowenia',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'31b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Szwecja',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'32b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Węgry',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'34b73cc6-28d6-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro Kurier DPD Włochy',
					'group'    => 'Allegro Standard - wysyłka z Polski za granicę',
					'subgroup' => 'Allegro Kurier DPD',
					'provider' => self::DPD,
				],
			'04b07522-70e5-4dc3-b13b-7918ea717539' =>
				[
					'name'     => 'Allegro One Kurier - dostawa dzisiaj (do 01.09.2022)',
					'group'    => 'Sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro One Kurier - dostawa dzisiaj (do 01.09.2022)',
					'provider' => self::ALLEGRO_ONE,
				],
			'17535ba6-077b-4371-ab4c-7918ea717539' =>
				[
					'name'     => 'Allegro One Kurier pobranie - dostawa dzisiaj (do 01.09.2022)',
					'group'    => 'Sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro One Kurier - dostawa dzisiaj (do 01.09.2022)',
					'provider' => self::ALLEGRO_ONE,
				],
			'8098f3dd-6d2f-4cdb-b317-9a2f986f2ec3' =>
				[
					'name'     => 'Allegro One Kurier - dostawa jutro (do 01.09.2022)',
					'group'    => 'Sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro One Kurier - dostawa jutro (do 01.09.2022)',
					'provider' => self::ALLEGRO_ONE,
				],
			'574d1c9e-9903-4626-903f-f72441d520d5' =>
				[
					'name'     => 'Allegro ORLEN Paczka',
					'group'    => 'Sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro ORLEN Paczka',
					'provider' => self::ORLEN_PACZKA,
				],
			'2503b262-e08c-4c2b-8598-38e0603d0d70' =>
				[
					'name'     => 'Allegro ORLEN Paczka pobranie',
					'group'    => 'Sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro ORLEN Paczka',
					'provider' => self::ORLEN_PACZKA,
				],
			'2a8cb7bf-a31b-45c3-b45e-7918ea717539' =>
				[
					'name'     => 'Allegro X-press Couriers (z odbiorem zużytego akumulatora)',
					'group'    => 'Sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro X-press Couriers (z odbiorem zużytego akumulatora)',
					'provider' => self::X_PRESS,
				],
			'3d7c74e7-4d1b-4800-95a2-7918ea717539' =>
				[
					'name'     => 'Allegro X-press Couriers (z odbiorem zużytego akumulatora) pobranie',
					'group'    => 'Sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro X-press Couriers (z odbiorem zużytego akumulatora)',
					'provider' => self::X_PRESS,
				],
			'7e113f1c-301a-406b-aa7a-b8654bc8c762' =>
				[
					'name'     => 'Allegro UPS Odbiór w Punkcie (do 06.09.2021)',
					'group'    => 'Sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro UPS Odbiór w Punkcie (do 06.09.2021)',
					'provider' => self::UPS,
				],
			'bfe96673-283f-40b3-b6ba-70940b16f7a4' =>
				[
					'name'     => 'Allegro UPS Odbiór w Punkcie (do 06.09.2021) pobranie',
					'group'    => 'Sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro UPS Odbiór w Punkcie (do 06.09.2021)',
					'provider' => self::UPS,
				],
			'8cf74350-94e8-4be7-ba3d-a736dd7621ec' =>
				[
					'name'     => 'Allegro Zadbano',
					'group'    => 'Przesyłki ciężkie i nieporęczne - sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro Zadbano',
					'provider' => self::ZADBANO,
				],
			'9b8e4329-92fe-479c-93de-778d33b2ff65' =>
				[
					'name'     => 'Allegro Zadbano pobranie',
					'group'    => 'Przesyłki ciężkie i nieporęczne - sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro Zadbano',
					'provider' => self::ZADBANO,
				],
			'ae814e65-d1e8-463a-85b2-4dff7e5146a7' =>
				[
					'name'     => 'Allegro SUUS Logistics',
					'group'    => 'Przesyłki ciężkie i nieporęczne - sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro SUUS Logistics',
					'provider' => self::SUUS,
				],
			'b7d7cc1b-04c9-4a47-be31-4dff7e5146a7' =>
				[
					'name'     => 'Allegro SUUS Logistics pobranie',
					'group'    => 'Przesyłki ciężkie i nieporęczne - sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro SUUS Logistics',
					'provider' => self::SUUS,
				],
			'a129b958-356f-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro No Limit',
					'group'    => 'Przesyłki ciężkie i nieporęczne - sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro No Limit',
					'provider' => self::NO_LIMIT,
				],
			'2e3782e0-3579-11eb-adc1-0242ac120002' =>
				[
					'name'     => 'Allegro No Limit pobranie',
					'group'    => 'Przesyłki ciężkie i nieporęczne - sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro No Limit',
					'provider' => self::NO_LIMIT,
				],
			'0ce12f52-e6aa-4ce5-a951-f54c1643bcb5' =>
				[
					'name'     => 'Allegro DHL Palety',
					'group'    => 'Przesyłki ciężkie i nieporęczne - sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro DHL Palety',
					'provider' => self::DHL,
				],
			'1655e292-7dd1-4d2f-b68b-f54c1643bcb5' =>
				[
					'name'     => 'Allegro DHL Palety pobranie',
					'group'    => 'Przesyłki ciężkie i nieporęczne - sposób rozliczeń Allegro Mix',
					'subgroup' => 'Allegro DHL Palety',
					'provider' => self::DHL,
				],
			'6a0f88ba-8f25-44fc-b1cf-25f45cbe8e72' =>
				[
					'name'     => 'Kurier wieczór',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier wieczór',
					'provider' => self::DHL,
				],
			'3ca09a1f-11f2-4763-974e-ae80404062c3' =>
				[
					'name'     => 'Kurier wieczór pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier wieczór',
					'provider' => self::DHL,
				],
			'7203cb90-864c-4cda-bf08-dc883f0c78ad' =>
				[
					'name'     => 'Przesyłka kurierska',
					'group'    => 'Kurier',
					'subgroup' => 'Przesyłka kurierska',
					'provider' => self::OTHER,
				],
			'845efe05-0c96-47c3-a8cb-aa4699c158ce' =>
				[
					'name'     => 'Przesyłka kurierska pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Przesyłka kurierska',
					'provider' => self::OTHER,
				],
			'00bc935e-b423-4cd4-9849-5760758db049' =>
				[
					'name'     => 'Kurier Ambro Express',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Ambro Express',
					'provider' => self::AMBRO,
				],
			'1fe8c93d-3a78-426d-8b91-5760758db049' =>
				[
					'name'     => 'Kurier Ambro Express pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Ambro Express',
					'provider' => self::AMBRO,
				],
			'0175efcd-8291-4328-ac41-258f5807b44e' =>
				[
					'name'     => 'Kurier Pocztex',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'1175efcd-15f3-4e97-8b15-258f5807b44e' =>
				[
					'name'     => 'Kurier Pocztex pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'4dd9c904-e892-4649-bdec-5454d6b53d28' =>
				[
					'name'     => 'Pocztex Kurier24',
					'group'    => 'Kurier',
					'subgroup' => 'Pocztex Kurier24',
					'provider' => self::POCZTA_POLSKA,
				],
			'b6bf5f9c-6cc8-4a0e-ab2f-bd49e76528f5' =>
				[
					'name'     => 'Pocztex Kurier24 pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Pocztex Kurier24',
					'provider' => self::POCZTA_POLSKA,
				],
			'f7e952b5-9ae8-40a9-90dd-e71ab9da29dd' =>
				[
					'name'     => 'Pocztex Kurier48',
					'group'    => 'Kurier',
					'subgroup' => 'Pocztex Kurier48',
					'provider' => self::POCZTA_POLSKA,
				],
			'8cc6e982-0af4-4e12-be96-5387db79a166' =>
				[
					'name'     => 'Pocztex Kurier48 pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Pocztex Kurier48',
					'provider' => self::POCZTA_POLSKA,
				],
			'1a228763-c17a-4b2d-88b0-63b082b04da6' =>
				[
					'name'     => 'Kurier InPost',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier InPost',
					'provider' => self::INPOST,
				],
			'999f8753-6340-48a0-8eba-46096f9749aa' =>
				[
					'name'     => 'Kurier InPost pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier InPost',
					'provider' => self::INPOST,
				],
			'b4525da7-fe10-4e89-af86-70479d3f251e' =>
				[
					'name'     => 'Kurier UPS',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier UPS',
					'provider' => self::UPS,
				],
			'496c5d07-a095-4cf3-bc3a-2217e01172a3' =>
				[
					'name'     => 'Kurier UPS pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier UPS',
					'provider' => self::UPS,
				],
			'059c0d58-6cdb-4955-ab79-9031518f80f3' =>
				[
					'name'     => 'Kurier GLS',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier GLS',
					'provider' => self::GLS,
				],
			'1205bb4a-7d74-46a4-b7de-1cb66a3d0c03' =>
				[
					'name'     => 'Kurier GLS pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier GLS',
					'provider' => self::GLS,
				],
			'c8be0338-63f9-42d8-bc78-ee2bd45980b6' =>
				[
					'name'     => 'Kurier DHL',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier DHL',
					'provider' => self::DHL,
				],
			'8de8c002-9f06-4e91-b8bc-aebcae3b9fe7' =>
				[
					'name'     => 'Kurier DHL pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier DHL',
					'provider' => self::DHL,
				],
			'c3066682-97a3-42fe-9eb5-9bb2978cf293' =>
				[
					'name'     => 'Kurier DPD',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier DPD',
					'provider' => self::DPD,
				],
			'259b5c7a-9056-4c74-80ec-9bb2978cf293' =>
				[
					'name'     => 'Kurier DPD pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier DPD',
					'provider' => self::DPD,
				],
			'881504b1-13e5-4280-b453-f1211164778e' =>
				[
					'name'     => 'Kurier FedEx',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier FedEx',
					'provider' => self::FEDEX,
				],
			'ba32c220-71e1-4df6-ab2b-7d5ff19a952d' =>
				[
					'name'     => 'Kurier FedEx pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier FedEx',
					'provider' => self::FEDEX,
				],
			'41cc412c-ba9e-422d-9750-7918ea717539' =>
				[
					'name'     => 'Kurier Pickpack',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Pickpack',
					'provider' => self::PICKPACK,
				],
			'e4d44e17-d107-447b-bebf-d82c636fe739' =>
				[
					'name'     => 'Kurier Raben',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Raben',
					'provider' => self::RABEN,
				],
			'25d687d5-5aab-41ef-afb6-c12ac1603c17' =>
				[
					'name'     => 'Kurier Raben pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Raben',
					'provider' => self::RABEN,
				],
			'ca225f8e-f5c5-42e9-a234-49fb629cd912' =>
				[
					'name'     => 'Kurier Rhenus',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Rhenus',
					'provider' => self::RHENUS,
				],
			'ec6f7e1f-dda2-4ef3-bd34-deac136e0b4b' =>
				[
					'name'     => 'Kurier Rhenus pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Rhenus',
					'provider' => self::RHENUS,
				],
			'07354b22-6484-4e90-b230-6a9dd91d95b2' =>
				[
					'name'     => 'Kurier DB Schenker',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier DB Schenker',
					'provider' => self::DB_SCHENKER,
				],
			'142f3a14-83c9-4aa0-8580-6a9dd91d95b2' =>
				[
					'name'     => 'Kurier DB Schenker pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier DB Schenker',
					'provider' => self::DB_SCHENKER,
				],
			'240f88db-7a7a-4d27-8004-9afbf05cfcd1' =>
				[
					'name'     => 'Kurier Geis',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Geis',
					'provider' => self::GEIS,
				],
			'3a9b10ce-abd5-4537-be33-9afbf05cfcd1' =>
				[
					'name'     => 'Kurier Geis pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Geis',
					'provider' => self::GEIS,
				],
			'42ea7239-28f2-4a0e-90da-e3934646bc7d' =>
				[
					'name'     => 'Kurier Patron Service',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Patron Service',
					'provider' => self::PATRON_SERVICE,
				],
			'5f439306-af7c-4371-9c88-e3934646bc7d' =>
				[
					'name'     => 'Kurier Patron Service pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Patron Service',
					'provider' => self::PATRON_SERVICE,
				],
			'6e68e08c-23d2-48f3-8d7a-c7e93bf55853' =>
				[
					'name'     => 'Kurier Pekaes',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Pekaes',
					'provider' => self::PEKAES,
				],
			'7be240c9-d718-4c0d-bfcc-c7e93bf55853' =>
				[
					'name'     => 'Kurier Pekaes pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier Pekaes',
					'provider' => self::PEKAES,
				],
			'84d052b8-e148-4f15-9581-93820784def1' =>
				[
					'name'     => 'Kurier TNT Express',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier TNT Express',
					'provider' => self::TNT_EXPRESS,
				],
			'954bc216-f562-48de-8e8e-93820784def1' =>
				[
					'name'     => 'Kurier TNT Express pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Kurier TNT Express',
					'provider' => self::TNT_EXPRESS,
				],
			'c9091ffb-727e-4a88-bd7c-b17dddbefb4a' =>
				[
					'name'     => 'Dostawa przez sprzedającego',
					'group'    => 'Kurier',
					'subgroup' => 'Dostawa przez sprzedającego',
					'provider' => self::OTHER,
				],
			'd1744bea-fb26-49d4-a2b9-2b59c25132e1' =>
				[
					'name'     => 'Dostawa przez sprzedającego pobranie',
					'group'    => 'Kurier',
					'subgroup' => 'Dostawa przez sprzedającego',
					'provider' => self::OTHER,
				],
			'1fa56f79-4b6a-4821-a6f2-ca9c16d5c925' =>
				[
					'name'     => 'Dostawa i jej koszt ustalane ze sprzedającym',
					'group'    => 'Kurier',
					'subgroup' => 'Inny sposób dostawy',
					'provider' => self::OTHER,
				],
			'02193188-23db-4017-8a22-6f93ea07ca13' =>
				[
					'name'     => 'Automat Pocztex',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Automat Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'b715fac1-8ec2-4f5c-8fdf-0f9cec9085ad' =>
				[
					'name'     => 'ORLEN Paczka',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'ORLEN Paczka',
					'provider' => self::ORLEN_PACZKA,
				],
			'121981e4-a14b-450b-b9fe-718df1dc3cd1' =>
				[
					'name'     => 'ORLEN Paczka pobranie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'ORLEN Paczka',
					'provider' => self::ORLEN_PACZKA,
				],
			'7b45d0c7-5e22-46ac-824e-f0384e9aab52' =>
				[
					'name'     => 'Paczka24 Odbiór w Punkcie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Paczka24 Odbiór w Punkcie',
					'provider' => self::POCZTA_POLSKA,
				],
			'b9e02dd2-6e91-4d13-901c-5e56ab0d1cd7' =>
				[
					'name'     => 'Paczka24 Odbiór w Punkcie pobranie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Paczka24 Odbiór w Punkcie',
					'provider' => self::POCZTA_POLSKA,
				],
			'07d4d067-2e02-4011-aa6e-491f6e8fe266' =>
				[
					'name'     => 'Odbiór w Punkcie Pocztex',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór w Punkcie Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'146b2c5d-d2f1-43b7-8268-491f6e8fe266' =>
				[
					'name'     => 'Odbiór w Punkcie Pocztex pobranie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór w Punkcie Pocztex',
					'provider' => self::POCZTA_POLSKA,
				],
			'd44995ed-edd2-4abb-999f-a56bd0f12a68' =>
				[
					'name'     => 'Punkty Poczta, Żabka, Orlen, Ruch',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Punkty Poczta, Żabka, Orlen, Ruch',
					'provider' => self::POCZTA_POLSKA,
				],
			'e36c9ccf-e341-49a9-8fa0-f073dcd61210' =>
				[
					'name'     => 'Punkty Poczta pobranie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Punkty Poczta, Żabka, Orlen, Ruch',
					'provider' => self::POCZTA_POLSKA,
				],
			'c576dc1d-4046-4b53-94de-47fc5b6913d4' =>
				[
					'name'     => 'Odbiór w Punkcie UPS',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór w Punkcie UPS',
					'provider' => self::UPS,
				],
			'e6d9d675-c8cb-4c0f-bcd2-febebfbac3d5' =>
				[
					'name'     => 'Odbiór w Punkcie UPS pobranie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór w Punkcie UPS',
					'provider' => self::UPS,
				],
			'685d8b40-2571-4111-8937-9220b1710d4c' =>
				[
					'name'     => 'Paczkomaty InPost',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Paczkomaty InPost',
					'provider' => self::INPOST,
				],
			'2653ca13-67c8-48c3-bbf8-ff9aa3f70ed3' =>
				[
					'name'     => 'Paczkomaty InPost pobranie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Paczkomaty InPost',
					'provider' => self::INPOST,
				],
			'afa68f62-0154-4556-9ee4-3f97c3c5bdca' =>
				[
					'name'     => 'Odbiór osobisty w punkcie sprzedawcy',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór osobisty w punkcie sprzedawcy',
					'provider' => self::OTHER,
				],
			'b6d6e4cf-67b3-4a4f-a38d-724335e9734f' =>
				[
					'name'     => 'Odbiór osobisty w punkcie sprzedawcy pobranie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór osobisty w punkcie sprzedawcy',
					'provider' => self::OTHER,
				],
			'57b987de-ce2d-4e73-9b9f-a7c9ebe9611d' =>
				[
					'name'     => 'Odbiór osobisty',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór osobisty',
					'provider' => self::OTHER,
				],
			'a6e4845b-9ace-48ea-b232-918eaf11ba6c' =>
				[
					'name'     => 'Odbiór osobisty pobranie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór osobisty',
					'provider' => self::OTHER,
				],
			'd66cfec5-12f6-452b-9a0c-16a02e717a11' =>
				[
					'name'     => 'Odbiór w punkcie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór w punkcie',
					'provider' => self::OTHER,
				],
			'f041aa82-9700-4248-aed0-df298f766b25' =>
				[
					'name'     => 'Odbiór w punkcie pobranie',
					'group'    => 'Odbiór w punkcie',
					'subgroup' => 'Odbiór w punkcie',
					'provider' => self::OTHER,
				],
			'2b6ca59d-1e4c-426c-82a9-efcbd730846b' =>
				[
					'name'     => 'Paczka pocztowa ekonomiczna',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'Paczka pocztowa ekonomiczna',
					'provider' => self::POCZTA_POLSKA,
				],
			'45309171-0415-49cd-b2cf-89e9143d20f0' =>
				[
					'name'     => 'Paczka pocztowa priorytetowa',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'Paczka pocztowa priorytetowa',
					'provider' => self::POCZTA_POLSKA,
				],
			'74bc07eb-552f-4581-b68c-da46716d4a9a' =>
				[
					'name'     => 'Paczka24',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'Paczka24',
					'provider' => self::POCZTA_POLSKA,
				],
			'b90c6295-b69a-4cb4-a308-7126a02aea47' =>
				[
					'name'     => 'Paczka24 pobranie',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'Paczka24',
					'provider' => self::POCZTA_POLSKA,
				],
			'ffb2643b-4b90-4925-9d29-0d93ad9488a6' =>
				[
					'name'     => 'Paczka48',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'Paczka48',
					'provider' => self::POCZTA_POLSKA,
				],
			'97286096-eb28-40f9-9efc-95ecbb8624ea' =>
				[
					'name'     => 'Paczka48 pobranie',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'Paczka48',
					'provider' => self::POCZTA_POLSKA,
				],
			'773167b1-feec-4ae9-b20f-1ed8ccb7b1ed' =>
				[
					'name'     => 'List polecony ekonomiczny',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'List polecony ekonomiczny',
					'provider' => self::POCZTA_POLSKA,
				],
			'758fcd59-fbfa-4453-ae07-4800d72c2ca5' =>
				[
					'name'     => 'List polecony priorytetowy',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'List polecony priorytetowy',
					'provider' => self::POCZTA_POLSKA,
				],
			'1fa56f79-4b6a-4821-a6f2-ca9c16d5c922' =>
				[
					'name'     => 'List ekonomiczny',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'List ekonomiczny',
					'provider' => self::POCZTA_POLSKA,
				],
			'bc2a5eb6-921f-4c1e-ab96-3a1b747ff9f6' =>
				[
					'name'     => 'List priorytetowy',
					'group'    => 'Paczki pocztowe i listy',
					'subgroup' => 'List priorytetowy',
					'provider' => self::POCZTA_POLSKA,
				],
		];

	/**
	 * @param $delivery_method_id
	 *
	 * @return string[]
	 * @throws UnknownDeliveryMethod
	 */
	public function get_delivery_method( $delivery_method_id ): array {
		if ( !isset( self::DELIVERY_METHODS[ $delivery_method_id ] ) ) {
			throw new UnknownDeliveryMethod();
		}

		return self::DELIVERY_METHODS[ $delivery_method_id ];
	}


}
