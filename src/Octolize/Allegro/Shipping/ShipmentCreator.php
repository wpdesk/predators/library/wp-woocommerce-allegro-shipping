<?php

namespace Octolize\Allegro\Shipping;

/**
 * Shipment creator.
 */
interface ShipmentCreator {

	/**
	 * @param \WC_Order $order
	 * @param string $transaction_id
	 * @param string $seller_id
	 * @param string $delivery_method_id
	 * @param bool $cod
	 * @param array $pickup_point
	 * @param array $allegro_checkout_form_data
	 *
	 * @return void
	 */
	public function create_shipment( $order, $transaction_id, $seller_id, $delivery_method_id, $cod, $pickup_point, $allegro_checkout_form_data = [] );

	/**
	 * @return array
	 */
	public function get_allegro_checkout_form_data();

}

