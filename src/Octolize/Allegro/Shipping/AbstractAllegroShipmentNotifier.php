<?php

namespace Octolize\Allegro\Shipping;

use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Cam notify WooCommerce Allegro plugin about created shipment.
 */
abstract class AbstractAllegroShipmentNotifier implements Hookable {

	const CARRIER_POCZTA_POLSKA = 'POCZTA_POLSKA';
	const CARRIER_INPOST = 'INPOST';
	const CARRIER_DPD = 'DPD';
	const CARRIER_DHL = 'DHL';

	/**
	 * @var string
	 */
	private $allegro_carrier_id;

	/**
	 * @var string
	 */
	private $shipment_status;

	/**
	 * @param string $allegro_carrier_id
	 * @param string $shipment_status
	 */
	public function __construct( string $allegro_carrier_id, string $shipment_status ) {
		$this->allegro_carrier_id = $allegro_carrier_id;
		$this->shipment_status    = $shipment_status;
	}

	/**
	 * @return void
	 */
	public function hooks() {
		add_action( 'flexible_shipping_shipment_status_updated', [ $this, 'notify_allegro_when_allegro_shipment' ], 10, 3 );
	}

	/**
	 * @param string $old_ststus
	 * @param string $new_status
	 * @param \WPDesk_Flexible_Shipping_Shipment $shipment
	 *
	 * @return void
	 */
	public function notify_allegro_when_allegro_shipment( $old_status, $new_status, $shipment ) {
		if ( $new_status === $this->shipment_status && $this->is_allegro_shipment( $shipment ) ) {
			do_action( 'woocommerce_allegro_shipment_notify', $shipment->get_order(), $this->allegro_carrier_id, $this->get_tracking_number( $shipment ) );
		}
	}

	/**
	 * @param \WPDesk_Flexible_Shipping_Shipment $shipment
	 *
	 * @return bool
	 */
	abstract protected function is_allegro_shipment( $shipment );

	/**
	 * @param \WPDesk_Flexible_Shipping_Shipment $shipment
	 *
	 * @return string
	 */
	abstract protected function get_tracking_number( $shipment );

}
