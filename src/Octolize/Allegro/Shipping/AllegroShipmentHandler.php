<?php

namespace Octolize\Allegro\Shipping;

use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Can handle notification from WooCommerce Allegro plugin and create shipment for order.
 */
class AllegroShipmentHandler implements Hookable {

	/**
	 * @var array<string, ShipmentCreator>
	 */
	private $shipment_data_creators;

	/**
	 * @param array<string, ShipmentCreator> $shipment_data_creators
	 */
	public function __construct( array $shipment_data_creators ) {
		$this->shipment_data_creators = $shipment_data_creators;
	}

	/**
	 * @return void
	 */
	public function hooks() {
		add_action( 'woocommerce_allegro_order_shipment', [ $this, 'create_shipment' ], 10, 3 );
	}

	/**
	 * @param \WC_Order $order
	 * @param string $allegro_seller_id
	 * @param array $allegro_checkout_form_data
	 *
	 * @return void
	 */
	public function create_shipment( $order, $allegro_seller_id, $allegro_checkout_form_data ) {
		if ( $order instanceof \WC_Order && $this->should_create_shipment( $order ) ) {
			$allegro_checkout_form_data = is_array( $allegro_checkout_form_data ) ? $allegro_checkout_form_data : [];
			$delivery_method_id         = $allegro_checkout_form_data['delivery']['method']['id'] ?? '';
			$cod                        = ( $allegro_checkout_form_data['payment']['type'] ?? '' ) === 'CASH_ON_DELIVERY';
			$pickup_point               = $allegro_checkout_form_data['delivery']['pickupPoint'] ?? [];
			$transaction_id             = $order->get_meta( '_allegro_checkout_id' );
			if ( isset( $this->shipment_data_creators[ $delivery_method_id ] ) ) {
				$this->shipment_data_creators[ $delivery_method_id ]->create_shipment( $order, $transaction_id, $allegro_seller_id, $delivery_method_id, $cod, $pickup_point, $allegro_checkout_form_data );
			}
		}
	}

	/**
	 * @param \WC_Order $order
	 *
	 * @return bool
	 */
	private function should_create_shipment( \WC_Order $order ): bool {
		return ! $this->order_has_shipments( $order );
	}

	/**
	 * @param \WC_Order $order
	 *
	 * @return bool
	 */
	private function order_has_shipments( \WC_Order $order ): bool {
		return ! empty( fs_get_order_shipments( $order->get_id() ) );
	}

}
