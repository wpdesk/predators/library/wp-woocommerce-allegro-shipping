<?php

namespace Octolize\Allegro\Shipping\Exceptions;

/**
 * Unknown Delivery Method.
 */
class UnknownDeliveryMethod extends \RuntimeException {
}
