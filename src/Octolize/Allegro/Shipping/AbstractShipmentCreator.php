<?php

namespace Octolize\Allegro\Shipping;

/**
 * Can create shipment for Allegro created order.
 */
abstract class AbstractShipmentCreator implements ShipmentCreator {

	/**
	 * @var string
	 */
	private $method_integration;

	/**
	 * @var string
	 */
	private $allegro_name;

	/**
	 * @var array
	 */
	private $allegro_checkout_form_data = [];

	/**
	 * @param string $method_integration
	 * @param string $allegro_name
	 */
	public function __construct( string $method_integration, string $allegro_name ) {
		$this->method_integration = $method_integration;
		$this->allegro_name = $allegro_name;
	}

	/**
	 * @return string
	 */
	public function get_allegro_name(): string {
		return $this->allegro_name;
	}

	/**
	 * @param \WPDesk_Flexible_Shipping_Shipment $shipment
	 * @param string $seller_id
	 * @param string $delivery_method_id
	 * @param \WC_Order $order
	 * @param bool $cod
	 * @param array $pickup_point
	 *
	 * @return void
	 */
	abstract protected function create_shipment_data( $order, $shipment, $transaction_id, $seller_id, $delivery_method_id, $cod, $pickup_point );

	/**
	 * Returns method integration for shipment.
	 *
	 * @return string
	 */
	private function get_method_integration(): string {
		return $this->method_integration;
	}

	/**
	 * @param \WC_Order $order
	 * @param string $transaction_id
	 * @param string $seller_id
	 * @param string $delivery_method_id
	 * @param bool $cod
	 * @param array $pickup_point
	 * @param array $allegro_checkout_form_data
	 *
	 * @return void
	 */
	public function create_shipment( $order, $transaction_id, $seller_id, $delivery_method_id, $cod, $pickup_point, $allegro_checkout_form_data = [] ) {
		$this->allegro_checkout_form_data = $allegro_checkout_form_data;
		$shipment = fs_create_shipment( $order, [ 'method_integration' => $this->get_method_integration() ] );
		$shipment->set_created_via( 'allegro-shipment-creator' );
		$this->create_shipment_data( $order, $shipment, $transaction_id, $seller_id, $delivery_method_id, $cod, $pickup_point );
		$shipment->save();
	}

	/**
	 * @return array
	 */
	public function get_allegro_checkout_form_data() {
		return $this->allegro_checkout_form_data;
	}

}
