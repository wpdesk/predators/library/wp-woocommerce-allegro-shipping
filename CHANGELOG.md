## [1.3.0] - 2022-10-31
### Added
- Allegro checkout form in shipment creator

## [1.2.0] - 2022-09-28
### Added
- allow to add shipment when the order has no other shipments

## [1.1.0] - 2022-09-26
### Added
- allegro shipping method name

## [1.0.0] - 2022-09-26
### Added
- initial version
